/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.services.creators;

import com.android.tools.idea.structure.services.DeveloperServiceCreator;
import com.android.tools.idea.structure.services.ServiceContext;
import com.google.services.GoogleServiceCreators;
import org.jetbrains.annotations.NotNull;

public final class IdentityServiceCreator extends DeveloperServiceCreator {
  @NotNull
  @Override
  protected String getResourceRoot() {
    return "/identity";
  }

  @NotNull
  @Override
  protected String[] getResources() {
    return new String[]{"logo_identity_color_2x_web_32dp.png", "recipe.xml", "service.xml"};
  }

  @Override
  protected void initializeContext(@NotNull ServiceContext serviceContext) {
    GoogleServiceCreators.initializeGoogleContext(serviceContext);
  }
}
